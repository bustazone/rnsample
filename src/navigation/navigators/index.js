import {LoginView} from "../../pages/login/LoginView";
import {SignUpView} from "../../pages/signup/SignUpView";
import {createStackNavigator} from "react-navigation";

export const RootNav = createStackNavigator({
    login: {
        screen: LoginView,
        navigationOptions: ({ navigation }) => ({
            title: "Login",
        }),
    },
    signup: {
        screen: SignUpView,
        // navigationOptions: ({ navigation }) => ({
        //     title: "1 Sign Up " + navigation.state.params.data,
        // }),
    }
}, {
    navigationOptions: {
        headerStyle: {
            backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    },
});