import React, {Component} from 'react';
import {RootNav} from "./navigation/navigators";
import {ItemDetailView} from "./pages/items_detail/ItemDetailView";
import {ItemsListView} from "./pages/items_list/ItemsListView";
import {LoginView} from "./pages/login/LoginView";
import {SignUpView} from "./pages/signup/SignUpView";


export default class App extends Component {
  render() {
    return (
        <RootNav/>
    );
  }
}