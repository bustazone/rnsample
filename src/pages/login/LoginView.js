import {Text, TouchableOpacity, View} from "react-native";
import React, {Component} from "react";
import styles from "./LoginView.Styles";

export class LoginView extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.test_button}
                    onPress={() => {
                    }}>
                    <Text>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.test_button}
                    onPress={() => this.props.navigation.navigate('signup', {
                        data: "Prueba",
                    })}>
                    <Text>SignUp</Text>
                </TouchableOpacity>
            </View>
        )
    }
}