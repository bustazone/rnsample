import {Text, TouchableOpacity, View} from "react-native";
import React, {Component} from "react";
import styles from "./FavoriteListView.Styles"

export class FavoriteListView extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.test_button}
                    onPress={() => {
                    }}>
                    <Text>Go to an item detail</Text>
                </TouchableOpacity>
            </View>
        )
    }
}