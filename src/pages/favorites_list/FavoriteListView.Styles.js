import {StyleSheet} from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#AA2222'
    },
    test_button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})