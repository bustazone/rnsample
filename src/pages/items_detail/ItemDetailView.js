import {Text, TouchableOpacity, View} from "react-native";
import React, {Component} from "react";
import styles from "./ItemDetailView.Styles";

export class ItemDetailView extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.test_button}
                    onPress={() => {
                    }}>
                    <Text>Mark As Favorite</Text>
                </TouchableOpacity>
            </View>
        )
    }
}