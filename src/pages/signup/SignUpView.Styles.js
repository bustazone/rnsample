import {Platform, StyleSheet} from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        ...Platform.select({
            ios: {backgroundColor: '#AA2222'},
            android: {backgroundColor: '#22AA22'}
        })
    },
    test_button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})