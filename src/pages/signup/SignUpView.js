import {Platform, Text, TouchableOpacity, View} from "react-native";
import React, {Component} from "react";
import styles from "./SignUpView.Styles";
import AuxView from "./AuxView";

export class SignUpView extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', '--------'),
        };
    };

    componentDidMount() {
        this.props.navigation.setParams({title: Platform.OS + " Sign Up"})
    }

    render() {
        const iOSText = <Text>iOS sign up :</Text>;
        const AndroidText = <Text>Android sign up :</Text>;
        return (
            <View style={styles.container}>
                {
                    Platform.select({
                        ios: iOSText,
                        android: AndroidText
                    })
                }
                <TouchableOpacity
                    style={styles.test_button}
                    onPress={() => {
                    }}>
                    <AuxView/>

                </TouchableOpacity>
            </View>
        )
    }
}